# Paneles
En Grafana tenemos diferentes paneles:

**General Status:**

- Este panel muestra información general del estado de la máquina-servidor y los datos totales si tiene varios hypervisores como el número de escritorios, plantillas...

**Hypervisors Status:**

- En este panel se pueden ver los logs de error, además de ver por cada hypervisor los escritorios, plantillas...

**Info Domains Full:**

- Este panel se utiliza si se requiere más información sobre la máquina, como por ejemplo sobre la memoria, del sistema...

**Info Hypervisors:**

- Parecido al panel de Info Domains Full, pero en este caso es una selección con paneles concretos que nos son útiles.

**IsardVDI Categories:**

- Panel para ver información por categoría de datos como los escritorios, plantillas...

**IsardVDI Containers:**

- Panel donde se puede ver el consumo RAM/CPU y tráfico de red de los contenedores Isard.

**IsardVDI Cost:**

- Panel que muestra el coste diario/mensual en oci además de datos generales para dar mejor vista del consumo.

**IsardVDI Monitoring:**

- Panel para monitorizar de manera rápida y sencilla los diferentes hypervisores con sus consumos y escritorios arrancados en cada uno.

**IsardVDI Rethinkdb:**

- Panel que contiene estadísticas, por ejemplo, número de consultas a la BBDD, número de conexiones...

**IsardVDI Storage:**

- Panel que muestra el espacio del disco en los diferentes puntos de montaje en porcentajes.

**IsardVDI Usage:**

- Este panel es donde podemos ver el uso que se está dando a Isard por categorías, el número de escritorios creados, plantillas creadas, número de usuarios creados...

**Paneles Gestor:**

- Este último panel está pensando específicamente para un usuario gestor que entre al Grafana, consta de varios paneles que solamente verá y no podrá modificar.