# Loki

### Grafana Loki es una herramienta que se usa para recopilar y visualizar los registros de aplicaciones y sistemas en tiempo real. En lugar de almacenar los registros en un solo lugar, los distribuye en múltiples nodos para mejorar la escalabilidad y la redundancia. Esto es especialmente útil en entornos de microservicios y contenedores, donde el volumen de registros puede ser muy grande y la infraestructura puede ser muy dinámica.

### En resumen, Grafana Loki es una herramienta útil para recopilar y visualizar registros de aplicaciones y sistemas en tiempo real para ayudar a detectar y resolver problemas más rápidamente.

> ***Como ejemplo se puede utilizar Loki para comprobar los logs de diferentes contenedores y revisar cuando haya algún problema de Isard como cuando no arranca correctamente una máquina y el mensaje de Error no parece ayudar, ver el procedimiento que ha seguido un desktop en concreto.***

- En la vista de administrador se puede extraer el identificador (Id) del escritorio, con `Ctr+Alt+i`, esto es de mucha ayuda al querer ver los logs solamente de ese escritorio:

    ![](https://i.imgur.com/FD5T8Ki.png)

- En el apartado `Explorer` de Grafana, se escoge el contenedor isard-engine que es donde se puede ver la actividad del escritorio:
    ```
    {container_name="isard-engine"} |= "_local-default-admin-admin_downloaded_slax93"
    ```
    ![](https://i.imgur.com/jiAZXpC.png)

- Registro de actividad del escritorio:

    ![](https://i.imgur.com/oSL8SZF.png)
