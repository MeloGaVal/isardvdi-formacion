# Alertas

## Ejemplos alertas
1. Para crear una alerta, hay que ir al apartado de `Alerting`, dentro de `Alert rules` y crear una nueva alerta en ![](https://i.imgur.com/L0vPrik.png) :

    ![](https://i.imgur.com/oflVtUS.png)


## Crear alerta por mensaje loki:
```
count_over_time({container_name="isard-engine"}  |= "'dead': ['disk_operations" [1m])
```

- Se selecciona el data source, en este caso **Loki**, para crear una alerta por mensaje de loki hay que seleccionar primero el contenedor donde aparezca el mensaje que queremos usar para crear la alerta, en este caso **isard-engine**, añadimos un **count_over_time** que hará un escaneo cada 1m de los logs de ese contenedor a ver si salta el mensaje que con un **Line contains** le indicamos que mensaje debería salir y alertar, esto se puede añadir manualmente o con ayuda del **Builder** de grafana:

    ![](https://i.imgur.com/xWKiVuO.png)

- Una vez hecho la query, seleccionamos las condiciones para la alerta sobre la query A y que esté por encima de 0, es decir que cada vez que salte el log del mensaje que queramos saltará la alerta:

    ![](https://i.imgur.com/TPGggON.png)

- En este apartado seleccionamos los tiempos de la alerta, el primer tiempo es para elegir cada cuanto se evalúa esta alerta y el segundo tiempo es una vez detecta la alerta y se queda en pending cuanto tarda en saltar en firing la alerta, es decir, en este caso sería: evalúa cada 30s y envía la alerta 30s después de detectarlo, en el siguiente apartado es para configurar los estados de la alerta, en primer lugar se selecciona que la alerta no cantará si no tiene ningún valor o es null el resultado, la segunda parte es para que al saltar la alerta lo marque como error:

    ![](https://i.imgur.com/HN25GP6.png)

- Este apartado sirve para darle un nombre a la alerta, crear o seleccionar la carpeta donde vayan las alertas y darle el nombre de grupo correspondiente para separar las alertas y darles diferentes configuraciones a la hora de lanzar las alertas como por ejemplo el tiempo:

    ![](https://i.imgur.com/NXSBygJ.png)

- Hay apartados opcionales como el `Summary and annotations`, estos apartados sirven para dar información extra o personalizada de lo que queramos mostrar en la alerta, en `Notifications` se pueden añadir **Labels** extra que aparezcan una vez tenga un valor determinado:
    ![](clipboard-202303061925-vfu4j.png)

## Crear alerta por superación de valor prometheus:
Es parecido al mensaje de loki, pero en este caso es para calcular la RAM en %, este cálculo se extrae de un contenedor llamado node-exporter, con esto podemos calcular la RAM y así poder hacer que cante una alterta cuando supera X % o valor:
```
100 - ((node_memory_MemAvailable_bytes{} * 100) / node_memory_MemTotal_bytes{})
```

![](https://i.imgur.com/w77dfqT.png)

- En este caso cantará la alerta cada vez que el % de la RAM supere el 80%:

    ![](https://i.imgur.com/9dYGk9S.png)

- Se añaden los campos con los valores que se desee:

    ![](https://i.imgur.com/K6psrxb.png)


## Exportar/Importar Alertas

1. Para extraer alertas que ya existen, primero generar un token temporal con los permisos de administrador, ir al grafana (/org/apikeys):

    ![](https://i.imgur.com/ux9kMGc.png) 

- Crear un nuevo token con role `Admin`:

    ![](https://i.imgur.com/RtLgtwS.png)

    ![](https://i.imgur.com/15EWRf6.png)

- Copiar el token:
  
    ![](https://i.imgur.com/JfYFTfh.png)

2. Coger el identificador de la alerta: 

    ![](https://i.imgur.com/wI4EhUR.png)

    ![](https://i.imgur.com/eIGshY3.png)


3. Lanzar el comando dentro del servidor donde esté corriendo el Grafana y así extraer el json, donde se remplaza con el `Token` de Grafana, el `Dominio` donde está arrancado y el `ID` de la alerta:
    ```
    curl -H "Authorization: Bearer <TOKEN>" <DOMAIN>/api/v1/provisioning/alert-rules/<ALERT ID> | jq`
    ```

>Este json posteriormente se guarda en el **PATH** donde se haya montado el isard `/opt/isard/monitor/grafana/custom/alerting`, así cada vez que arranque el Grafana tendrá las alertas ya cargadas y sin tener que volver a ponerlas a mano.

## Conectar: Alertas de Grafana + TelegramBot
### Añadir un contact point

1. En el menú de Grafana, haz clic en el ícono de `Alertas` (campana) para abrir la página de Alertas que muestra las alertas existentes.

2. Haz clic en `Contact Point` para abrir la página que muestra los puntos de contacto existentes.

3. Haz clic en `New Contact Point`.

4. En el menú desplegable Alertmanager, selecciona un Alertmanager. Por defecto, se selecciona Grafana Alertmanager.

5. En Nombre, ingresa un nombre descriptivo para el contact point.

6. En Tipo de Contact Point, se selecciona el tipo y rellenas los campos obligatorios. Por ejemplo, si eliges correo electrónico, introduce las direcciones de correo electrónico. O si eliges Telegram introduce el Token del Bot y el Chat ID.

7. Algunos tipos de punto de contacto, como correo electrónico o webhook, tienen configuraciones opcionales. En Configuraciones opcionales, se especifica configuraciones adicionales para el tipo de punto de contacto que hayas puesto.

8. En Configuraciones de notificación, opcionalmente puedes seleccionar Desactivar mensaje resuelto si no quieres recibir una notificación cuando una alerta se resuelva.

9.  Haz clic en Save contact point para guardar los cambios de los contact points añadidos anteriormente.

## Crear un punto de contacto de Telegram

Necesitarás un token de API BOT y un CHAT ID.

1. Abre Telegram y crea un nuevo bot buscando a @BotFather y luego escribiendo /newbot.

2. Sigue las indicaciones para crear un nombre y un nombre de usuario para tu bot. El nombre de usuario debe terminar en "bot" y debe ser único.

3. Te darán un token de API HTTP que es tu token de API BOT que se usará en Grafana. Tendrá el formato XXXXXXXXX: YYYYYYYYYYYYYYYYYYYYYYYYYYYYY.

4. Luego necesitarás el CHAT ID. Para obtenerlo, primero debes crear un grupo y luego agregar el nuevo bot al mismo.

5. Luego, presiona la opción "Ver información del grupo" para el grupo, haz clic en el nombre de usuario del bot en la lista de miembros y presiona el botón "ENVIAR MENSAJE".

6. A continuación, envía cualquier mensaje en el grupo donde hayas añadido el bot.

7. Después, en tu navegador, visita la URL https://api.telegram.org/botXXX:YYY/getUpdates (reemplaza XXX:YYY con el token de API de BOT que acabas de obtener de Telegram). 
   En la respuesta JSON, deberías ver un nodo con un mensaje que tenga el type=group. Este nodo también tendrá un ID. Copia el ID en el campo de CHAT ID en Grafana.

8. Ahora puedes probar el nuevo punto de contacto de Telegram en Grafana utilizando el botón "Test".

    Debería haber una alerta de ejemplo de Grafana dentro de la ventana de mensajes del nuevo grupo de Telegram.
