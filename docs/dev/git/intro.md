# Git

## Qué es?

Git es un sistema de control de versiones. Es decir, permite hacer un seguimiento sobre los cambios realizados sobre el código fuente.

## Que nos permite?

**Colaborar de manera eficiente**
Los programadores pueden realizar modificaciones sobre diferentes partes del código en paralelo y fusionarlos de manera sencilla.

**Comparar versiones del código.** Puede compararse el contenido del código y ver las diferencias entre dos momentos determinados.

**Recuperar versiones pasadas.** Los programadores tienen acceso a un historial completo del código, por lo que pueden retroceder a cualquier punto anterior en el tiempo.

## Instalación

Fedora:

`$ yum install git`

Distribuciones basadas en Debian:

`$ apt-get install git`

Referencia: https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Instalaci%C3%B3n-de-Git