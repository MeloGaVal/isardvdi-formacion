# Alta disponibilidad en IsardVDI

En IsardVDI usamos la réplica de datos DRBD entre los servidores controlado, junto a otros servicios, por el gestor de recursos en clúster Pacemaker.

La instalación del gestor Pacemaker incluye la instalación del servicio *corosync* que monitoriza constantemente los servidores y sus servicios definidos.

![img/pacemaker_drbd.png](img/pacemaker_drbd.png)

*Reference: https://abelog.tech/archives/189*

## DRBD

Nos proporcionará una copia exacta de los bloques de un dispositivo de almacenamiento en otros servidores. En caso de caida se levantará el dispositivo de bloques DRBD en otro de los servidores réplica.

Como usamos en IsardVDI el DRBD es habitualmente con un único servidor con el dispositivo de bloques DRBD en primario, es decir que se puede montar en un directorio.

En el servidor en dónde está en primario podremos usar el dispotivo DRBD como cualquier otro disco, creando particiones y sistemas de ficheros. Todos los cambios se sincronizan al momento en el otro servidor, de aquí la importancia de una conexión de red con baja latencia.

Hay una evolución de la versión 8 (solo permitía hasta tres nodos y una única conexión) a la versión 9 que usaremos que permite hacer conexionados de malla hasta 16 nodos.

![img/drbd8vs9.webp](img/drbd8vs9.webp)

*Referencia: https://blog.drbd.jp/2015/04/drbd9-3-multinode/*

## Pacemaker

Es el servicio (*pcsd*) que gestionará y monitorizará los recursos del clúster mientras las comunicaciones y detección de estado del clúster serán gestionadas por el servicio *corosyncd*.

![img/pacemaker_corosync.png](img/pacemaker_corosync.png)

*Referencia: https://wiki.myhypervisor.ca/books/linux/page/drbd-pacemaker-corosync-mysql-cluster-centos7*

Una vez instalado el pacemaker definiremos los recursos de los que debe hacerse cargo, como por ejemplo un servicio web *nginx*, una bbdd *mysql* o también el recurso primario del DRBD.

Para que una vez el primario cambie de un servidor a otro, porque se ha detectado un fallo en el servicio o en el servidor y sus comunicaciones se iniciará la secuencia de migración de servicios:

1. Primario DRBD 
2. Montaje en directorio
3. Reinicio en el nuevo nodo de los recursos

En el caso que se pierdan las comunicaciones no podrá hacer la secuencia de reincio de servicios en uno de los restantes nodos sin asegurarse que en el nodo desaparecido los recursos se han parado. Podría comportar corrupción de datos no hacerlo (imaginemos una BBDD que se arrancara con los mismos datos en dos servidores distintos a la vez).

Por eso es básico para el Pacemaker un sistema para asegurar que el nodo desaparecido no tiene recursos activos. Usa una PDU (stonith), que no es más que un sitema de relés controlados por red que permiten dar o quitar corriente en cada enchufe.

![img/stonith.png](img/stonith.png)

*Referencia: https://ourobengr.com/ha/* (Este está en inglés ;-) y explica bien el tema)

El primer nodo que detecte la problemática y que consiga llegar primero al dispositivo de relés *stonith* será el que sobreviva.

Un ejemplo del estado de un clúster pacemaker con DRBD, NFS, IP flotante y recursos docker compose.

```
Cluster name: UniCluster
Stack: corosync
Current DC: nodo1 (version 2.0.1-9e909a5bdd) - partition with quorum
Last updated: Wed Mar 22 22:26:23 2023
Last change: Wed Mar 22 22:26:19 2023 by root via cibadmin on nodo3

3 nodes configured
38 resources configured

Online: [ nodo1 nodo2 nodo3 ]

Full list of resources:

 stonith1	(stonith:fence_ipmilan):	Started nodo3
 stonith2	(stonith:fence_ipmilan):	Started nodo1
 stonith3	(stonith:fence_ipmilan):	Started nodo1
 Clone Set: drbd-clone [drbd] (promotable)
     Masters: [ nodo3 ]
     Slaves: [ nodo1 nodo2 ]
 Resource Group: storage
     lvm	(ocf::heartbeat:LVM):	Started nodo3
     mountfs	(ocf::heartbeat:Filesystem):	Started nodo3
     nfs_ip	(ocf::heartbeat:IPaddr2):	Started nodo3
     nfs-daemon	(ocf::heartbeat:nfsserver4):	Started nodo3
     nfs-root	(ocf::heartbeat:exportfs):	Started nodo3
     nfs-isard	(ocf::heartbeat:exportfs):	Started nodo3
     out-ip	(ocf::heartbeat:IPaddr2):	Started nodo3
     infra-ip	(ocf::heartbeat:IPaddr2):	Started nodo3
 Clone Set: isard-clone [isard]
     Started: [ nodo3 ]
     Stopped: [ nodo1 nodo2 ]
 Clone Set: hypervisors-clone [hypervisors]
     Started: [ nodo1 nodo2 ]
     Stopped: [ nodo3 ]

Daemon Status:
  corosync: active/enabled
  pacemaker: active/enabled
  pcsd: active/enabled
```